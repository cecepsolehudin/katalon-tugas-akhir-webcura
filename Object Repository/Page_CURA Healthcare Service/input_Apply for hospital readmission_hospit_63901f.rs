<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Apply for hospital readmission_hospit_63901f</name>
   <tag></tag>
   <elementGuidId>41311fef-fe3b-435c-927d-6bbfa80d67ee</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#chk_hospotal_readmission</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='chk_hospotal_readmission']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>abe51b26-84b3-495d-9523-dbd0e6b32728</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>9cacf530-b48e-447b-a208-bcbe74cca66c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>chk_hospotal_readmission</value>
      <webElementGuid>e987fd32-617b-4dd3-8d86-e54b29f3b917</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>hospital_readmission</value>
      <webElementGuid>2a9cde40-5f26-4391-8924-82522b6ca61f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>Yes</value>
      <webElementGuid>f551773e-ecaf-4c01-922a-dc201f64020f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;chk_hospotal_readmission&quot;)</value>
      <webElementGuid>510c94ec-2366-45b1-8ba3-0ebc429af8df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='chk_hospotal_readmission']</value>
      <webElementGuid>dfbb03b1-78c5-4e1f-8572-dea677bf8151</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[2]/div/label/input</value>
      <webElementGuid>6cfc79ac-d0b4-4f1d-94fc-7fc3de702238</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>79a294e9-7293-4583-b693-45c996ba9550</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @id = 'chk_hospotal_readmission' and @name = 'hospital_readmission']</value>
      <webElementGuid>f433fac5-3238-4342-896e-94b586237103</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
