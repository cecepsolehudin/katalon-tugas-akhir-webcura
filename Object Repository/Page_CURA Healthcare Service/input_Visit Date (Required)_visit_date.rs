<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Visit Date (Required)_visit_date</name>
   <tag></tag>
   <elementGuidId>458f6421-6a83-4ea6-a72f-8f4ada8baafb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#txt_visit_date</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='txt_visit_date']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>fb13bafd-1735-413e-ac0c-d4e054636ca3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>c2329398-2ea9-4e15-8f23-bb10b5bbd6ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>873fb4b6-55e2-475e-89e8-e0206eabaee4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>txt_visit_date</value>
      <webElementGuid>99f2312e-3f43-41ec-8edc-b2868a69e234</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>visit_date</value>
      <webElementGuid>5fb85ba8-118e-43ba-b7f9-51cd0c9e3c07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>dd/mm/yyyy</value>
      <webElementGuid>dcbd2591-84dc-4d2b-9ec2-405d6263d521</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>dc8c3673-9e34-4f70-af48-24587a9d0620</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;txt_visit_date&quot;)</value>
      <webElementGuid>280619fa-2a14-40a4-a867-7b73d38fe040</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='txt_visit_date']</value>
      <webElementGuid>2d4e7979-b138-4e7e-aaad-b25e84c3f5fb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//section[@id='appointment']/div/div/form/div[4]/div/div/input</value>
      <webElementGuid>fd1ab555-4110-4d77-a1a6-5c70c1e5b346</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>fe5c7c61-19c8-454d-969e-cd98d896a9a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @id = 'txt_visit_date' and @name = 'visit_date' and @placeholder = 'dd/mm/yyyy']</value>
      <webElementGuid>75b5998c-be2f-4f9c-b887-bcf8b8073054</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
